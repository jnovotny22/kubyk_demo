namespace Anf.Stb.Library.UI.Controls
{
	public interface INotifyPropertyChanged : Object
	{
		public signal void property_changed(Object sender, PropertyChangedEventArgs args);
	}
}
