using Anf.Stb.Library.Common;

namespace Anf.Stb.Library.UI.Controls
{
	public class PropertyChangedEventArgs : EventArgs
	{
		private string _property_name;

		public string property_name 
		{
			get { return _property_name; }
			set { _property_name = value; }
		}
	}
}
