namespace Anf.Stb.Library.UI.Controls
{
	public interface ICommand : Object
	{
		public abstract bool is_enabled { get; }
		public abstract void execute(); 
	}
}
