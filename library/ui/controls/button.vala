using GLib;
using Gee;
using Clutter;
using Mx;

namespace Anf.Stb.Library.UI.Controls
{
	public class Button : Mx.Button, ICommanding
	{
        private weak ICommand _button_click_command;

		//TODO maca: Why the fuck cannot chain the constructors with base class Mx.Button?

    	public Button()
		{
			/*
            class MyWindow : Gtk.Window {
				  public MyWindow () {
					      Object (type: Gtk.WindowType.TOPLEVEL);
						    }
			}
			*/

			GLib.Object();
			hook_events();
		}

		public Button.with_label(string label_text)
		{
			GLib.Object(label: label_text);
			hook_events();
		}

		public virtual ICommand default_action_command
		{
			get
			{
				return _button_click_command;
			}
			set
			{
				_button_click_command = value;
				clicked.connect(() => {});
			}
		}

		private void hook_events()
		{
			this.clicked.connect(button_clicked_commanding_handler);
		}

		private void button_clicked_commanding_handler()
		{
			if(_button_click_command != null)
			{
				_button_click_command.execute();
			}
		}
	}
}
