using Anf.Stb.Library.Common;

namespace Anf.Stb.Library.UI.Composite
{
	public interface IEvent : Object
	{
        public enum EventScope
		{
			GLOBAL,
			WORKITEM,
			CHILDREN,
			CHILDREN_AND_PARENTS
		}

		public delegate void EventTopicHandlerFunc(Object sender,
			EventArgs args,
			IEvent event_topic,
		    EventScope scope,
			WorkItem original_workitem);
	}
}
