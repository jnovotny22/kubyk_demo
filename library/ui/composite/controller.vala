namespace Anf.Stb.Library.UI.Composite
{
	public abstract class Controller : Object
	{
        public enum StatusType
		{
			INACTIVE,
			ACTIVE
		}

		private StatusType _status = StatusType.INACTIVE;
        private WorkItem _workitem;

		public WorkItem workitem
		{
			get { return _workitem; }
			private set { _workitem = value; }
		}

		public StatusType status
		{
			get { return _status; }
			private set { _status = value; }
		}

		public Controller(WorkItem workitem)
		{
			this.workitem = workitem;
		}

       	public virtual void activate()
		{
			if(status != StatusType.ACTIVE)
			{
				status = StatusType.ACTIVE;
				on_activated();
			}
		}

		protected virtual void on_activated()
		{
		}
	}
}
