using GLib;
using Gee;
using Clutter;
using Mx;

namespace Anf.Stb.Library.UI.Composite
{
	public class Application : Object
	{
        private const int DEFAULT_STAGE_WIDTH = 800;
		private const int DEFAULT_STAGE_HEIGHT = 600;

        private WorkItem _root_workitem;
		private static Application _current_application;
		private Stage _main_stage;
		private bool _is_main_stage_active;
		private int? _exit_code;
		private bool _is_main_stage_initialized;
		private Controller _main_controller;

		public WorkItem root_workitem
		{
			get { return _root_workitem; }
			private set { _root_workitem = value; }
		}

		public Stage main_stage
		{
			get { return _main_stage; }
			private set { _main_stage = value; }
		}

		public bool is_main_stage_active
		{
			get { return _is_main_stage_active; }
			private set { _is_main_stage_active = value; }
		}

		public int? exit_code
		{
			get { return _exit_code; }
			private set { _exit_code = value; }
		}

		public bool is_main_stage_initialized
		{
			get { return _is_main_stage_initialized; }
			private set { _is_main_stage_initialized = value; }
		}

		public Controller? main_controller
		{
			get { return _main_controller; }
			private set { _main_controller = value; }
		}

		public virtual void run(ref unowned string[] args)
		{
			message("Application starting.");

            if(_current_application != null)
			{
				error("Another application is already running");
				throw new ApplicationException.ANOTHER_APPLICATION_ALREADY_RUNNING("Another application is already running.");
			}

			_current_application = this;
			initialize_environment(ref args);
			root_workitem = create_root_workitem();
			register_components(root_workitem);
			main_stage.show_all();
			main_controller = create_main_controller(root_workitem);
			enter_main_loop(args);
		}

		protected virtual Controller? create_main_controller(WorkItem woritem)
		{
			return null;
		}

		protected virtual void enter_main_loop(string[] args)
		{
			message("Entering main Clutter loop.");
			Clutter.main();
		}

		protected virtual void initialize_environment(ref unowned string[] args)
		{
            message("Initializing environment");

			var clutter_init_result = Clutter.init(ref args);
			if(clutter_init_result != Clutter.InitError.SUCCESS)
			{
				error("Cannot initialize clutter.");
				exit();
			}

			main_stage = create_main_stage();
			configure_main_stage(main_stage);
		}

		protected virtual void configure_main_stage(Stage stage)
		{
			message("Configuring main stage");

			//TODO maca: enable this in production
			//stage.set_fullscreen(true);            
			stage.width = DEFAULT_STAGE_WIDTH;
			stage.height = DEFAULT_STAGE_HEIGHT;
		}

		protected virtual Stage create_main_stage()
		{
			message("Creating main stage");
			var stage = new Clutter.Stage();
			stage.activate.connect(stage_activate_event_handler);
			stage.deactivate.connect(stage_deactivate_event_handler);
			stage.delete_event.connect(stage_delete_event_handler);
			stage.captured_event.connect(stage_captured_event_handler);

			return stage;
		}

		private bool stage_captured_event_handler(Event event)
		{
			return on_main_stage_event(event);
		}

		private void stage_activate_event_handler()
		{
			var old_value = is_main_stage_active;
			is_main_stage_active = true;
			if(old_value != is_main_stage_active)
			{
				on_main_stage_active_changed(is_main_stage_active);
			}
		}

		private void stage_deactivate_event_handler()
		{
			var old_value = is_main_stage_active;
			is_main_stage_active = false;
			if(old_value != is_main_stage_active)
			{
				on_main_stage_active_changed(is_main_stage_active);
			}
		}

		private bool stage_delete_event_handler(Event event)
		{
			bool cancel = false;
			on_main_stage_deleting(ref cancel);
			if(!cancel)
			{
				on_main_stage_deleted();
			}
			return cancel;
		}

		protected virtual void on_main_stage_active_changed(bool is_active)
		{
			message("Main stage %s"
				.printf(is_active 
			   		 ? "activated" 
					: "deactivated"));
		  	
			if(!is_main_stage_initialized)
			{
				is_main_stage_initialized = true;
				on_main_stage_initialized();
			}
		}

		protected virtual void on_main_stage_initialized()
		{
			if(main_controller != null)
			{
				main_controller.activate();
			}
		}

		protected virtual void on_main_stage_deleting(ref bool cancel)
		{
			debug("Main stage is trying to terminate");
		}

		protected virtual void on_main_stage_deleted()
		{
			message("Main stage has been terminated");
			exit();
		}

		protected virtual bool on_main_stage_event(Event event)
		{
			return false;
		}

		protected virtual WorkItem create_root_workitem()
		{
			message("Creating root workitem");
			return new WorkItem();
		}

		protected virtual void register_components(WorkItem workitem)
		{
			message("Registering components");
		}

		public virtual void exit()
		{
			if(exit_code != null)
			{
				message("Exiting with code %d".printf(exit_code));
			}
			else
			{
				message("Exiting");
			}

			Clutter.main_quit();
		}

		public virtual void exit_with_code(int code)
		{
			exit_code = code;
			exit();
		}
	}
}
