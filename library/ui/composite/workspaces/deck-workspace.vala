using Clutter;
using Mx;

namespace Anf.Stb.Library.UI.Composite.Workspaces
{
	public class DeckWorkspace : Actor, IWorkspace
	{
		public virtual void show_view(IView view)
		{
        	show_view_with_workspace_metadata(view, new WorkspaceShowMetadata());
		}

		public virtual void show_view_with_workspace_metadata(IView view, WorkspaceShowMetadata workspace_show_metadata)
		{
			if(view is Actor)
			{
				add((Actor)view);
			} 
		}
	}
}
