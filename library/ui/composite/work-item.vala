using GLib;
using Gee;
using Anf.Stb.Library.Common;
using Anf.Stb.Library.Common.Collections;

namespace Anf.Stb.Library.UI.Composite
{
	/**
	 * This class represents single unit-of-work pattern element in composite UI
	 * structure.
	 *
	 * Use this class to register items that are necessary to accomplish an use-case
	 * logic. Register services, views, commands, event-topics that are either accessible
	 * at current level of business logic or that should be accessible in some nested
	 * use-cases.
	 */
	public class WorkItem : Object
	{
        public enum StatusType
		{
			IDLE,
			ACTIVE,
			TERMINATED
		}

		/**
		 * Contains handling delegate.
		 * This class is redundant but since Vala is a complete shit and cannot handle
		 * delegates as generic parameters, this is the only way how to achieve a hash_map
		 * containing delegates.
		 */
		private class EventTopicHandler : Object
		{
			private IEvent.EventTopicHandlerFunc _handling_func;

			/**
			 * Gets the handling function.
			 */
			public IEvent.EventTopicHandlerFunc handling_func
			{
				get { return _handling_func; }
			}

			/**
			 * Creates new EventTopicHandler/
			 *
			 * @param handling_func
			 *	Handling function that will be stored with this EventTopicHandler wrapper.
			 */
			public EventTopicHandler(IEvent.EventTopicHandlerFunc handling_func)
			{
				_handling_func = handling_func;
			}
		}

		// Parent workitem
		private weak WorkItem _parent;

		// Child workitems
		private FeedbackArrayList<WorkItem> _children;

		// Registered workspaces on local level
		private FeedbackHashMap<string, IWorkspace> _workspaces;

		// Registered services on local level
		private FeedbackHashMap<string, IService> _services;

		// Registered views on local level
		private FeedbackHashMap<string, IView> _views;

		// Registered commands on local level
		private FeedbackHashMap<string, ICommand> _commands;

		// Registered events on local level
		private FeedbackHashMap<string, IEvent> _events;

		// State-bag
		private HashMap<string, Object> _state;

		// Local subscribed events
		private HashMap<weak IEvent, ArrayList<EventTopicHandler>> _subscribed_events;

		private StatusType _status;

		/**
		 * Creates new workitem.
		 */
		public WorkItem()
		{
			_children = new FeedbackArrayList<WorkItem>();
			_children.collection_changed.connect(children_changed_event_handler);

			_subscribed_events = new HashMap<weak IEvent, ArrayList<EventTopicHandler>>();

			_workspaces = new FeedbackHashMap<string, IWorkspace>
				.with_traversing_delegate(workspace_traversing_func);
			_services = new FeedbackHashMap<string, IService>
				.with_traversing_delegate(service_traversing_func);
			_views = new FeedbackHashMap<string, IView>
				.with_traversing_delegate(view_traversing_func); 
			_commands = new FeedbackHashMap<string, ICommand>
				.with_traversing_delegate(command_traversing_func); 
			_events = new FeedbackHashMap<string, IEvent>
				.with_traversing_delegate(event_traversing_func);

			_views.collection_changed.connect(managed_collection_changed_event_handler);
			_workspaces.collection_changed.connect(managed_collection_changed_event_handler);
			_services.collection_changed.connect(managed_collection_changed_event_handler);
			_commands.collection_changed.connect(managed_collection_changed_event_handler);
			_events.collection_changed.connect(managed_collection_changed_event_handler);
		}

		/**
		 * Gets parent workitem.
		 */
		public WorkItem parent
		{
			get { return _parent; }
			private set { _parent = value; }
		}

		/**
		 * Gets children workitems
		 */
		public ArrayList<WorkItem> workitems
		{
			get { return _children; }
		} 

		/**
		 * Gets local workspaces
		 */
		public HashMap<string, IWorkspace> workspaces
		{
			get { return _workspaces; }
		}                   

		/**
		 * Gets hierarchival services storage accessible by service name.
		 * You can access all the items registered in parent (transitive closure) workitems
		 * as well as local ones.
		 * You can override any single value on current workitem level simply by setting
		 * an item with the same key as in any of parent workitems.
		 * Note that you cannot add two or more items to same collection sharing the same key
		 * in one workitem level.
		 */
		public HashMap<string, IService> services
		{
			get { return _services; }
		}

		/**
		 * Gets hierarchival views storage accessible by service name.
		 * You can access all the items registered in parent (transitive closure) workitems
		 * as well as local ones.
		 * You can override any single value on current workitem level simply by setting
		 * an item with the same key as in any of parent workitems.
		 * Note that you cannot add two or more items to same collection sharing the same key
		 * in one workitem level.
		 */
		public HashMap<string, IView> views
		{
			get { return _views; }
		}

		/**
		 * Gets hierarchival commands storage accessible by service name.
		 * You can access all the items registered in parent (transitive closure) workitems
		 * as well as local ones.
		 * You can override any single value on current workitem level simply by setting
		 * an item with the same key as in any of parent workitems.
		 * Note that you cannot add two or more items to same collection sharing the same key
		 * in one workitem level.
		 */
		public HashMap<string, ICommand> commands
		{
			get { return _commands; }
		}

		/**
		 * Gets hierarchival events storage accessible by service name.
		 * You can access all the items registered in parent (transitive closure) workitems
		 * as well as local ones.
		 * You can override any single value on current workitem level simply by setting
		 * an item with the same key as in any of parent workitems.
		 * Note that you cannot add two or more items to same collection sharing the same key
		 * in one workitem level.
		 */
		public HashMap<string, IEvent> events
		{
			get { return _events; }
		}

		/**
		 * Gets the state bag.
		 * Use this to get/set stateful information for this workitem, like use-case discriminators
		 * and other stateful information.
		 */
		public HashMap<string, Object> state
		{
			get
			{
				if(_state == null)
				{
					_state = new HashMap<string, Object>();
				}

				return _state;
			}
		}

		public StatusType status
		{
			get { return _status; }
			private set { _status = value; }
		}

		/**
		 * Gets the subscibed events for this workitem
		 */
		private HashMap<weak IEvent, ArrayList<EventTopicHandler>> subscribed_events
		{
			get
			{
				return _subscribed_events;
			}
		}

 		private void managed_collection_changed_event_handler(Object sender, CollectionChangedEventArgs args)
		{
			if(args is HashMapChangedEventArgs)
			{
				var hashmap_changed_event_args = (HashMapChangedEventArgs)args;
				foreach(var item in hashmap_changed_event_args.new_items)
				{
					var key_value_pair = (KeyValuePair<string, Object>)item;
					if(key_value_pair.value is IWorkItemAware)
					{
						var workitem_aware = (IWorkItemAware)key_value_pair.value;
						manage_connected_workitem_aware_component(workitem_aware);
					}
				}
			}
		}

		private void manage_connected_workitem_aware_component(IWorkItemAware workitem_aware)
		{
			workitem_aware.set_workitem(this);
		}

		/**
		 * Handles collection-changed event of workitems collection to check, whether the workitem
		 * has not been linked to any other workitem.
		 * Also chains the workitems together.
		 *
		 * @param args
		 *	collection-changed event args
		 */
		private void children_changed_event_handler(CollectionChangedEventArgs<WorkItem> args)
		{
			if(args.change_type == CollectionChangedEventArgs.ChangeType.ADD
					|| args.change_type == CollectionChangedEventArgs.ChangeType.ITEM_CHANGED)
			{
				assert(args.new_items != null);
				assert(args.new_items.size > 0);

				ListIterator<WorkItem> iterator = args.new_items.list_iterator();
				while(iterator.next())
				{
					WorkItem child_workitem = iterator.get();
					if(child_workitem.parent == this && workitems.contains(child_workitem))
					{
						// We allow to re-connect workitem to chain if its in legal bounds, eg.
						// It is reconecting to it's parent again.
						continue;
					}

					// This is wrong, cannot chain already chained workitem. May lead to cycles.
					if(child_workitem.parent != null)
					{
						throw new WorkItemException.WORKITEM_ALREADY_CHAINED("WorkItem has been already added to some workitem tree.");
					}
				}
			}

			// Link new child workitems
			if(args.old_items.size > 0)
			{
				foreach(var old_item in args.old_items)
				{
					old_item.parent = null;
				}
			}

			// Unlink old child workitems
			if(args.new_items.size > 0)
			{
				foreach(var new_item in args.new_items)
				{
					new_item.parent = this;
				} 
			}
		}

		/**
		 * Traverses backwards in hierarchy to sweep all workspaces.
		 *
		 * @param parent_map
		 *	map to search parent in
		 * @return
		 *	parent map
		 */
		private TraversingHashMap<string, IWorkspace> workspace_traversing_func(TraversingHashMap<string, IWorkspace> parent_map)
		{
			return parent != null
				? (TraversingHashMap<string, IWorkspace>)parent.workspaces
				: null;
		}

		/**
		 * Traverses backwards in hierarchy to sweep all services.
		 *
		 * @param parent_map
		 *	map to search parent in
		 * @return
		 *	parent map
		 */
		private TraversingHashMap<string, IService> service_traversing_func(TraversingHashMap<string, IWorkspace> parent_map)
		{
			return parent != null
				? (TraversingHashMap<string, IService>)parent.services
				: null;
		}

		/**
		 * Traverses backwards in hierarchy to sweep all views.
		 *
		 * @param parent_map
		 *	map to search parent in
		 * @return
		 *	parent map
		 */
		private TraversingHashMap<string, IView> view_traversing_func(TraversingHashMap<string, IWorkspace> parent_map)
		{
			return parent != null
				? (TraversingHashMap<string, IView>)parent.views
				: null;
		}

		/**
		 * Traverses backwards in hierarchy to sweep all commands.
		 *
		 * @param parent_map
		 *	map to search parent in
		 * @return
		 *	parent map
		 */
		private TraversingHashMap<string, ICommand> command_traversing_func(TraversingHashMap<string, IWorkspace> parent_map)
		{
			return parent != null
				? (TraversingHashMap<string, ICommand>)parent.commands
				: null;
		}

		/**
		 * Traverses backwards in hierarchy to sweep all events.
		 *
		 * @param parent_map
		 *	map to search parent in
		 * @return
		 *	parent map
		 */
		private TraversingHashMap<string, IEvent> event_traversing_func(TraversingHashMap<string, IWorkspace> parent_map)
		{
			return parent != null
				? (TraversingHashMap<string, IEvent>)parent.events
				: null;
		}

		/**
		 * Gets all event-handlers subscribed to that particular event-topic.
		 *
		 * @param event-topic
		 *	For this event topic all the handlers will be returned.
		 * @return
		 *	Collection of event-handlers for this topic.
		 */
		private Collection<EventTopicHandler> get_event_handlers(IEvent event_topic)
		{
			var result = new ArrayList<EventTopicHandler>();
			if(subscribed_events.contains(event_topic))
			{
				foreach(var event_topic_handler in subscribed_events.get(event_topic))
				{
					result.add(event_topic_handler);
				}
			}

			return result;
		}

		/**
		 * Gets all the event-handlers in workitem hierarchy.
		 *
		 * @return 
		 *	All the event-handlers in workitem hierarchy.
		 */
		private Collection<EventTopicHandler> get_global_event_handlers(IEvent event_topic)
		{
			return get_node_children_event_handlers(get_root_workitem(), event_topic);
		}

		/**
		 * Gets all child event-handlers for one workitem.
		 * All event-handlers starting this one (including local handlers) and all the children's 
		 * handlers will be returned.
		 *
		 * @param start
		 *	Workitem that will be traversed.
		 * @param event_topic
		 *	Event topic that will be used to find all it's event-handlers.
		 * @return
		 *	Collection of child event-handlers for that one event-topic.
		 */
		private Collection<EventTopicHandler> get_node_children_event_handlers(WorkItem start, IEvent event_topic)
		{
			var result = new ArrayList<EventTopicHandler>();
			var queue = new ArrayList<WorkItem>();
			queue.add(start);
			while(queue.size > 0)
			{
				var current = queue.remove_at(0);
				foreach(var child_workitem in current.workitems)
				{
					queue.add(child_workitem);
				}

				result.add_all(current.get_local_event_handlers(event_topic));
			}

			return result;
		}

		/**
		 * Gets local handlers for one event-topic.
		 *
		 * @param event-topic
		 *	Topic which the event-handlers will be searched for.
		 * @return
		 * 	Collection of local event handlers for that one event-topic.
		 */
		private Collection<EventTopicHandler> get_local_event_handlers(IEvent event_topic)
		{
			var result = new ArrayList<EventTopicHandler>();
			if(subscribed_events.contains(event_topic))
			{
				result.add_all(subscribed_events.get(event_topic));
			}

			return result;
		}

		/**
		 * Gets the all handlers up the hierarchy. 
		 * Handlers for the starting workitem will not be returned.
		 *
		 * @param node
		 *	Starting node
		 * @param event-topic
		 * 	Topic for which the handler will be returned.
		 */
		private Collection<EventTopicHandler> get_node_parent_event_handlers(WorkItem node, IEvent event_topic)
		{
			var result = new ArrayList<EventTopicHandler>();
			var current = node;
			while(current.parent != null)
			{
				result.add_all(current.parent.get_local_event_handlers(event_topic));
				current = current.parent;
			}

			return result;
		}

		/**
		 * Internal logic of event firing.
		 *
		 * @param event_topic
		 *	Topic that will be fired.
		 * @param sender
		 *	Event originator.
		 * @param args
		 *	Event arguments.
		 * @param scope
		 *	Event scope.
		 * @param original_workitem
		 *	The firing workitem
		 */
		private void fire_event_internal(IEvent event_topic,
				Object sender,
				EventArgs args,
				IEvent.EventScope scope,
				WorkItem original_workitem)
		{
			var handlers = new ArrayList<EventTopicHandler>();

			if(scope == IEvent.EventScope.GLOBAL)
			{
				handlers.add_all(get_global_event_handlers(event_topic));	
			}
			else
			{
				if(scope == IEvent.EventScope.CHILDREN
						|| scope == IEvent.EventScope.CHILDREN_AND_PARENTS)
				{
					handlers.add_all(get_node_children_event_handlers(this, event_topic));
				}

				if(scope == IEvent.EventScope.CHILDREN_AND_PARENTS)
				{
					handlers.add_all(get_node_parent_event_handlers(this, event_topic));
				}

				get_local_event_handlers(event_topic);
			}


			foreach(var handler in handlers)
			{
				handler.handling_func(sender,
						args,
						event_topic,
						scope,
						this);
			}
		}

		/**
		 * Gets the root workitem.
		 *
		 * @return
		 *	Root workitem
		 */
		public WorkItem get_root_workitem()
		{
			WorkItem current = this;
			while(current.parent != null)
			{
				current = current.parent;
			}

			return current;
		}

		/**
		 * Fires the event-topic that will be spreading from this workitem.
		 *
		 * @param event_topic
		 *	Topic that will be fired.
		 * @param sender
		 *	Event originator.
		 * @param args
		 *	Event arguments.
		 * @param scope
		 *	Event scope
		 */
		public virtual void fire_event(IEvent event_topic,
				Object sender,
				EventArgs args,
				IEvent.EventScope scope)
		{
			fire_event_internal(event_topic,
					sender,
					args,
					scope,
					this);
		}

		/**
		 * Subscribes event-handler to event-topic.
		 *
		 * When an event is fired and this workitem is in it's scope, all event-handlers to that event-topic
		 * will be fired.
		 * The order of the event-handling is undeterministic.
		 *
		 * @param topic
		 *	Event topic this handler will be subscribed to.
		 * @param handler
		 *	Handling function for that particular event-topic
		 */
		public virtual void subscribe_event_handler(IEvent topic, IEvent.EventTopicHandlerFunc handler)
		{
			if(!subscribed_events.has_key(topic))
			{
				subscribed_events.set(topic, new ArrayList<EventTopicHandler>());
			}

			var list = subscribed_events.get(topic);
			var subscribed_event_handler = new EventTopicHandler(handler);
			list.add(subscribed_event_handler);
		}

		/**
		 * Unsubscribes all event-handlers for this particular event-topic in this workitem.
		 *
		 * @param topic
		 * 	All event-handlers for this topic will be unsubscribed in this workitem.
		 */
		public virtual void unsubscribe_event(IEvent topic)
		{
			if(subscribed_events.has_key(topic))
			{
				subscribed_events.unset(topic);
			}
		}

		/**
		 * Unsubscribes an event-handler for one particular event-topic.
		 *
		 * @param topic
		 *	Event topic that current handler will be unsubscribed from.
		 * @param handler
		 *	Handler to be unsubscribed from event-topic
		 */
		public virtual void unsubscribe_event_handler(IEvent topic, IEvent.EventTopicHandlerFunc handler)
		{
			if(subscribed_events.has_key(topic))
			{
				var handlers = subscribed_events.get(topic);
				var matching_handlers = new ArrayList<EventTopicHandler>();
				foreach(var event_topic_handler in handlers)
				{
					if(event_topic_handler.handling_func == handler)
					{
						matching_handlers.add(event_topic_handler);
					}
				}

				foreach(var handler_to_remove in matching_handlers)
				{
					handlers.remove(handler_to_remove);
				}
			}
		}

		public virtual void activate()
		{
			if(status == StatusType.TERMINATED)
			{
				throw new WorkItemException.WORKITEM_ALREADY_TERMINATED("WorkItem has been already terminated.");
			}

			if(status == StatusType.IDLE)
			{
				on_activated();
				status = StatusType.ACTIVE;
			}
		}

		public virtual void terminate()
		{
			if(status == StatusType.TERMINATED)
			{
 				throw new WorkItemException.WORKITEM_ALREADY_TERMINATED("WorkItem has been already terminated.");
			}

			var cancel = false;
         	on_terminating(ref cancel);
			if(!cancel)
			{
				terminate_internal();
				on_terminated();
			}
		}

		private void terminate_internal()
		{
			foreach(var workitem in workitems)
			{
				workitem.terminate();
			}

			terminate_managed_items();
		}

		private void terminate_managed_items()
		{
        	terminate_managed_collection(_views.values);
			terminate_managed_collection(_services.values);
			terminate_managed_collection(_workspaces.values);
			terminate_managed_collection(_events.values);
			terminate_managed_collection(_commands.values);
		}

		private void terminate_managed_collection(Collection<Object> collection)
		{
			//TODO maca: Do another cleanup
			collection.clear();
		}

		protected virtual void on_activated()
		{
			fire_activated(new EventArgs());
		} 

		protected virtual void on_terminating(ref bool cancel)
		{
			var args = new BeforeEventArgs();
			fire_terminating(args);
			cancel = args.cancel;
		}

		protected virtual void on_terminated()
		{
			fire_terminated(new EventArgs());
		}

		private void fire_activated(EventArgs args)
		{
			activated(this, args);
		}

		private void fire_terminating(BeforeEventArgs args)
		{
			terminating(this, args);
		}

		private void fire_terminated(EventArgs args)
		{
			terminated(this, args);
		}

		public signal void activated(Object sender, EventArgs args);
		public signal void terminating(Object sender, BeforeEventArgs args);
		public signal void terminated(Object sender, EventArgs args);
	}
}
