using Anf.Stb.Library.UI.Controls;

namespace Anf.Stb.Library.UI.Composite.Commands
{
	public delegate void DelegatingCommandHandlerFunc();
	public delegate bool DelegatingCommandIsEnabledFunc();

	public class DelegatingCommand : Object,
	Anf.Stb.Library.UI.Controls.ICommand,
	Anf.Stb.Library.UI.Composite.ICommand,
	INotifyPropertyChanged
	{
		public const string PROPERTY_IS_ENABLED = "is_enabled";

		private bool? _is_enabled_value;
		private DelegatingCommandHandlerFunc _handler_func;
		private DelegatingCommandIsEnabledFunc _is_enabled_func;

		public DelegatingCommand(DelegatingCommandHandlerFunc handler_func,
				DelegatingCommandIsEnabledFunc is_enabled_func)
		{
			_handler_func = handler_func;
			_is_enabled_func = is_enabled_func;
		}

		public virtual void execute()
		{
			_handler_func();
		}

		public virtual bool is_enabled
		{
			get
			{
				if(_is_enabled_value == null)
				{
					_is_enabled_value = _is_enabled_func();
				}

				return _is_enabled_value;
			}
		}

		public virtual void invalidate()
		{
			var old_value = _is_enabled_value;
			_is_enabled_value = null;
			var new_value = is_enabled;
			if(old_value != new_value)
			{
				fire_property_changed(PROPERTY_IS_ENABLED);
			}
		}

		private void fire_property_changed(string property)
		{
			var args = new PropertyChangedEventArgs();
			args.property_name = property;
			property_changed(this, args);
		}
	}
}
