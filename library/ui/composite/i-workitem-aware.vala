namespace Anf.Stb.Library.UI.Composite
{
	public interface IWorkItemAware : Object
	{
		public abstract void set_workitem(WorkItem workitem);
	}
}
