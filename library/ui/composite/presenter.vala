using Clutter;

namespace Anf.Stb.Library.UI.Composite
{
	public abstract class Presenter : Object
	{
    	private weak IView _view;
		private WorkItem _workitem;

		public Presenter(IView view, WorkItem workitem)
		{
			_view = view;
			_workitem = workitem;
			view.set_presenter(this);
			if(view is Actor)
			{
				hook_on_actor_view((Actor)view);
			}
		}

		public IView view
		{
			get { return _view; }
		}

		public WorkItem workitem
		{
			get { return _workitem; }
		}
		
		protected virtual void hook_on_actor_view(Actor view)
		{
			view.realize.connect(view_realize_handler);

			if(view is Actor)
			{
				var actor = (Actor)view;
				actor.destroy.connect(view_destroy_handler);
			}
		}

		private void view_realize_handler()
		{
			if(view is Actor)
			{
				((Actor)view).realize.disconnect(view_realize_handler);
			}

			on_initialized();
		}
  
		private void view_destroy_handler()
		{
			on_destroyed();
		}

		protected virtual void on_initialized()
		{
			message("Presenter %s initialized"
				.printf(this.get_type().name()));
		}

		protected virtual void on_destroyed()
		{
        	if(view is Actor)
			{
				var actor = (Actor)view;
				actor.destroy.disconnect(view_destroy_handler);
			}

			message("Presented %s destroyed"
				.printf(this.get_type().name()));
		}
	}
}
