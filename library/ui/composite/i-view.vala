namespace Anf.Stb.Library.UI.Composite
{
	public interface IView : Object
	{
		public abstract void set_presenter(Presenter presenter);
	}
}
