using GLib;
using Gee;

namespace Anf.Stb.Library.UI.Composite
{
	public interface IWorkspace : Object
	{
		public abstract void show_view(IView view);
		public abstract void show_view_with_workspace_metadata(IView view, WorkspaceShowMetadata workspace_show_metadata);
	}
}

