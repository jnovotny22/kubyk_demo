using Gee;

namespace Anf.Stb.Library.Common.Collections
{
	public class FeedbackArrayList<G> : ArrayList<G>, ICollectionChangedReporter
	{
		private void fire_collection_changed(CollectionChangedEventArgs args)
		{
			collection_changed(args);
		}

		protected virtual void on_collection_changed(CollectionChangedEventArgs args)
		{
			fire_collection_changed(args);
		}

		public override void set(int index, G item)
		{
			base.set(index, item);
			var old_items = new ArrayList<G>();
			old_items.add(this.get(index));
			var new_items = new ArrayList<G>();
			new_items.add(item);
			var args = new CollectionChangedEventArgs<G>.with_old_and_new(CollectionChangedEventArgs.ChangeType.ITEM_CHANGED,
					old_items,
					new_items);
			on_collection_changed(args);
		}

		public override bool add(G item)
		{
			var result = base.add(item);
			if(result)
			{
				var new_items = new ArrayList<G>();
				new_items.add(item);
				var args = new CollectionChangedEventArgs<G>.with_old_and_new(CollectionChangedEventArgs.ChangeType.ADD,
						new ArrayList<G>(),
						new_items);
				on_collection_changed(args);
			}
			return result;
		}

		public override void insert(int index, G item)
		{ 
			base.insert(index, item);
			var new_items = new ArrayList<G>();
			new_items.add(item);
			var args = new CollectionChangedEventArgs<G>.with_old_and_new(CollectionChangedEventArgs.ChangeType.ADD,
					new ArrayList<G>(),
					new_items);
			on_collection_changed(args); 
		}

		public override bool remove(G item)
		{
			var result = base.remove(item);
			if(result)
			{
				var old_items = new ArrayList<G>();
				old_items.add(item);
				var args = new CollectionChangedEventArgs<G>.with_old_and_new(CollectionChangedEventArgs.ChangeType.REMOVE,
						old_items,
						new ArrayList<G>());
				on_collection_changed(args);
			}
			return result;
		}

		public override G remove_at(int index)
		{
			var result = base.remove_at(index);
			if(result != null)
			{
				var old_items = new ArrayList<G>();
				old_items.add(result);
				var args = new CollectionChangedEventArgs<G>.with_old_and_new(CollectionChangedEventArgs.ChangeType.REMOVE,
						old_items,
						new ArrayList<G>());
				on_collection_changed(args);
			}
			return result;
		}

		public override void clear()
		{
			if(this.size > 0)
			{
				var old_items = new ArrayList<G>();
				old_items.add_all(this);
				base.clear();
				var args = new CollectionChangedEventArgs<G>.with_old_and_new(CollectionChangedEventArgs.ChangeType.RESET,
						old_items,
						new ArrayList<G>());
				on_collection_changed(args);
			}
		}

		public override bool add_all(Collection<G> collection)
		{
			var result = base.add_all(collection);
			if(result)
			{
				var new_items = new ArrayList<G>();
				new_items.add_all(collection);
				var args = new CollectionChangedEventArgs<G>.with_old_and_new(CollectionChangedEventArgs.ChangeType.ADD,
						new ArrayList<G>(),
						new_items);
				on_collection_changed(args);
			}
			return result;
		}
	}
}
