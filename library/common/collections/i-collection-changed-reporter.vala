using GLib;
using Gee;

namespace Anf.Stb.Library.Common.Collections
{
	public interface ICollectionChangedReporter : Object
	{
		public abstract signal void collection_changed(CollectionChangedEventArgs args);
	}
}
