namespace Anf.Stb.Library.Common.Collections
{
	public class KeyValuePair<K, V> : Object
	{
		private K _key;
		private V _value;

		public KeyValuePair()
		{
		}

		public KeyValuePair.with_key_value(K key, V value)
		{
			this();
			_key = key;
			_value = value;
		}

		public K key
		{
			get { return _key; }
			set { _key = value; }
		}

		public V value
		{
			get { return _value; }
			set { _value = value; }
		}
	}
}
