namespace Anf.Stb.Library.Common.Collections
{
	public class HashMapChangedEventArgs<K, V> : CollectionChangedEventArgs<KeyValuePair<K, V>>
	{
		public HashMapChangedEventArgs(CollectionChangedEventArgs.ChangeType type)
		{
			base(type);
		}

		public HashMapChangedEventArgs.with_old_and_new(CollectionChangedEventArgs.ChangeType type,
			Gee.List<KeyValuePair<K, V>> cold,
			Gee.List<KeyValuePair<K, V>> cnew)
		{
			base.with_old_and_new(type, cold, cnew);
		}
	}
}
