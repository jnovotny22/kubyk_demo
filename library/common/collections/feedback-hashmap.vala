using GLib;
using Gee;

namespace Anf.Stb.Library.Common.Collections
{
	public class FeedbackHashMap<K, V> : TraversingHashMap<K, V>, ICollectionChangedReporter
	{
        public FeedbackHashMap()
		{
			base();
		}

		public FeedbackHashMap.with_traversing_delegate(owned HashMapTraversalFunc<K, V> traversal_func)
		{
			base.with_traversing_delegate(traversal_func);
		}

		public override void set(K key, V value)
		{
			V old = null;
			if(contains(key))
			{
				old = get(key);
			}

			base.set(key, value);
			var cnew = new ArrayList<KeyValuePair<K, V>>();
			var cold = new ArrayList<KeyValuePair<K, V>>();
			var key_value = new KeyValuePair<K, V>.with_key_value(key, value);
			cnew.add(key_value);
			if(old != null)
			{
				cold.add(new KeyValuePair<K, V>.with_key_value(key, old));
			}

			var args = new HashMapChangedEventArgs<K, V>.with_old_and_new(
					old != null 
					? CollectionChangedEventArgs.ChangeType.ITEM_CHANGED
					: CollectionChangedEventArgs.ChangeType.ADD,
					cold,
					cnew);

			on_hashmap_changed(args);
		}

		public override bool unset(K key, out V value)
		{
			value = null;
			var result = base.unset(key, out value);
			if(result)
			{
				var cnew = new ArrayList<KeyValuePair<K, V>>();
				var cold = new ArrayList<KeyValuePair<K, V>>();
				cold.add(new KeyValuePair<K, V>.with_key_value(key, value));
				var args = new HashMapChangedEventArgs<K, V>.with_old_and_new(CollectionChangedEventArgs.ChangeType.REMOVE,
						cold,
						cnew);
				on_hashmap_changed(args);
			}

			return result;
		}

		public override void clear()
		{
			if(size > 0)
			{
				var cold = new ArrayList<KeyValuePair<K, V>>();
				var cnew = new ArrayList<KeyValuePair<K, V>>();
				var iterator = map_iterator();
				while(iterator.next())
				{
					var key = iterator.get_key();
					var value = iterator.get_value();
					cold.add(new KeyValuePair<K, V>.with_key_value(key, value));
				}
				var args = new HashMapChangedEventArgs<K, V>.with_old_and_new(
						CollectionChangedEventArgs.ChangeType.RESET,
						cold,
						cnew);
				on_hashmap_changed(args);
			}
		}

		protected virtual void on_hashmap_changed(CollectionChangedEventArgs args)
		{
			fire_hashmap_changed(args);
		}

		private void fire_hashmap_changed(CollectionChangedEventArgs args)
		{
			hashmap_changed(this, args);
		}

		public signal void hashmap_changed(Object sender, CollectionChangedEventArgs args);
	}
}
