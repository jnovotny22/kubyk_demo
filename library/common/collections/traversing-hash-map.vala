using GLib;
using Gee;

namespace Anf.Stb.Library.Common.Collections
{
	public delegate TraversingHashMap<K, V>? HashMapTraversalFunc<K, V>(TraversingHashMap<K, V> parent);

	public class TraversingHashMap<K, V> : HashMap<K, V>
	{
		private HashMapTraversalFunc<K, V> _traversal_func;

		public TraversingHashMap()
		{
			base();
		}

		public TraversingHashMap.with_traversing_delegate(owned HashMapTraversalFunc<K, V> traversal_func)
		{
			this();
			_traversal_func = traversal_func;
		}

		public override bool has_key(K key)
		{
			if(base.has_key(key))
			{
				return true;
			}

			if(_traversal_func != null)
			{
				var parent = _traversal_func(this);
				if(parent != null)
				{
					return parent.has_key(key);
				} 
				else 
				{
					return false;
				}
			}

			return false;
		}

		public override bool has(K key, V value)
		{
			if(base.has(key, value))
			{
				return true;
			}

			if(_traversal_func != null)
			{
				var parent = _traversal_func(this);
				if(parent != null)
				{
					return parent.has(key, value);
				} 
				else 
				{
					return false;
				}
			}

			return false;
		}

		public override V get(K key)
		{
			V result;
			if((result = base.get(key)) != null)
			{
				return result;
			}

			if(_traversal_func != null)
			{
				var parent = _traversal_func(this);
				if(parent != null)
				{
					return parent.get(key);
				} 
				else 
				{
					return null;
				}
			}

			return null;
		}
	}
}
