using Gee;

namespace Anf.Stb.Library.Common.Collections
{
	/**
	 * Argumets for collection-changed event.
	 */
	public class CollectionChangedEventArgs<G> : EventArgs
	{
		/**
		 * Defines the collection change type
		 */
		public enum ChangeType
		{
			/**
			 * Occures when one or more items is replaced by another one.
			 * New collection will hold new items.
			 * Old collection will hold replaced items.
			 */
			ITEM_CHANGED,

			/**
			 * Occures when one or more items has been removed.
			 * New collection will be empty.
			 * Old collection will hold removed items.
			 */
			REMOVE,

			/**
			 * Occures when one or more items has been added.
			 * New collection will hold all the new items.
			 * Old collection will be empty.
			 */
			ADD,

			/**
			 * Occures when the collection has been re-set.
			 * New collection will be empty.
			 * Old collection will hold all the items, that were contained within the collection
			 * before reset occured.
			 */
			RESET
		}

		private ChangeType _type;
		private Gee.List<G> _old;
		private Gee.List<G> _new;

		/**
		 * Creates new args instance.
		 *
		 * @param type
		 * 	Change type.
		 */
		public CollectionChangedEventArgs(ChangeType type)
		{
			_type = type;
		}

		/**
		 * Creates new args instance.
		 *
		 * @param type
		 *	Change type.
		 * @param cold
		 *	Old collection.
		 * @param cnew
		 *	New collection.
		 */
		public CollectionChangedEventArgs.with_old_and_new(ChangeType type,
			Gee.List<G> cold,
			Gee.List<G> cnew)
		{
			this(type);
			_old = cold;
			_new = cnew;
		}

		/**
		 * Gets/sets the change type.
		 */
		public ChangeType change_type
		{
			get { return _type; }
			set { _type = value; }
		}

		/**
		 * Gets/sets the old items.
		 * Those items have been removed from collection.
		 */
		public Gee.List<G> old_items
		{
			get { return _old; }
			set { _old = value; }
		}

		/**
		 * Gets/sets the new items.
		 * Those items have been added to collection.
		 */
		public Gee.List<G> new_items
		{
			get { return _new; }
			set { _new = value; }
		}
	}
}
