namespace Anf.Stb.Library.Common
{
	public class BeforeEventArgs : EventArgs
	{
		private bool _cancel;

		public bool cancel
		{
			get { return _cancel; }
			set { _cancel = value; }
		}
	}
}
