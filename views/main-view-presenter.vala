using Anf.Stb.Library;
using Anf.Stb.Library.Common;
using Anf.Stb.Library.Common.Collections;
using Anf.Stb.Library.UI;
using Anf.Stb.Library.UI.Composite;

namespace Anf.Stb.Demo.Views
{
	public class MainViewPresenter : Presenter
	{
		private ICommand _hello_world_command;

		public MainViewPresenter(IView view, WorkItem workitem)
		{
			base(view, workitem);
		}

		public ICommand get_hello_world_command()
		{
			return workitem.commands.get("HelloWorldCommand");
		}
	}
}


