using GLib;
using Gee;
using Clutter;
using Mx;
using Anf.Stb.Library;
using Anf.Stb.Library.Common;
using Anf.Stb.Library.Common.Collections;
using Anf.Stb.Library.UI;
using Anf.Stb.Library.UI.Controls;
using Anf.Stb.Library.UI.Composite;
using Anf.Stb.Services;

namespace Anf.Stb.Demo.Views
{
	public class MainView : Actor, IView
	{
		MainViewPresenter _presenter;
		private Anf.Stb.Library.UI.Controls.Button _btn_hello_world;

		public MainView()
		{ 
			message("Creating MyView");
			MainMenuItem menu_item = new MainMenuItem();
			MainMenuDownloader main_menu = new MainMenuDownloader();
			
			ArrayList<string> icon_paths;
			ArrayList<string> selected_icon_paths;
			int icon_count =  main_menu.download_data(out icon_paths,out selected_icon_paths);
			message(icon_count.to_string()+" icons downloaded");
			if (icon_paths.size != selected_icon_paths.size)
			{
				message("Different size of icon and selected icon list");				
			}
			else
			{
				
			}
		}

		public virtual void set_presenter(Presenter presenter)
		{
			assert(presenter is MainViewPresenter);
			_presenter = (MainViewPresenter)presenter;
			_btn_hello_world.default_action_command = _presenter.get_hello_world_command();			
		}
	}
}
