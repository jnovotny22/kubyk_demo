# Makefile
# vala project
#
# name of your project/program
PROGRAM = main_debug
 
# for most cases the following two are the only you'll need to change
# add your source files here
SRC =       main.vala \
		   	library/common/event-args.vala \
			library/common/before-event-args.vala \
			library/common/collections/i-collection-changed-reporter.vala \
			library/common/collections/hashmap-changed-event-args.vala \
			library/common/collections/collection-changed-event-args.vala \
			library/common/collections/feedback-array-list.vala \
			library/common/collections/traversing-hash-map.vala \
			library/common/collections/key-value-pair.vala \
			library/common/collections/feedback-hashmap.vala \
			library/ui/controls/property-changed-event-args.vala \
			library/ui/controls/i-notify-property-changed.vala \
			library/ui/controls/i-commanding.vala \
			library/ui/controls/i-command.vala \
			library/ui/controls/button.vala \
			library/ui/controls/main-menu-item.vala \
			library/ui/composite/i-workitem-aware.vala \
			library/ui/composite/work-item-exception.vala \
			library/ui/composite/application-exception.vala \
			library/ui/composite/workspace-show-metadata.vala \
			library/ui/composite/i-workspace.vala \
			library/ui/composite/i-view.vala \
			library/ui/composite/i-command.vala \
			library/ui/composite/i-event.vala \
			library/ui/composite/i-service.vala \
			library/ui/composite/work-item.vala \
			library/ui/composite/application.vala \
			library/ui/composite/presenter.vala \
			library/ui/composite/controller.vala \
			library/ui/composite/workspaces/deck-workspace.vala \
			infrastructure/workspace-names.vala \
			demo-application.vala \
			controllers/main-controller.vala \
			views/main-view.vala \
			views/main-view-presenter.vala \
			library/ui/composite/commands/delegating-command.vala \
			services/http_manager.vala \
			services/main-menu-downloader.vala \
			services/i-menu-downloader.vala \
			services/xml_parser.vala \
			services/cache_data.vala

SRC_C = utils/main.c

# comments 
# add your used packges here
PKGS = --pkg gtk+-2.0 \
	 --pkg gee-1.0 \
	 --pkg libxml-2.0 \
	 --pkg libsoup-2.4 \
	 --pkg gio-2.0 \
	 --pkg clutter-1.0 \
	 --pkg mx-1.0 \
	 --pkg json-glib-1.0 \
	 --pkg sqlite3
	 
# to compile the app from the c code to the stb
STB_LIBS = $(shell pkg-config --cflags --libs clutter-gdl-1.0 gtk+-2.0 gobject-2.0 gee-1.0 libxml-2.0 libsoup-2.4 gio-2.0 sqlite3) 

# application name in STB
STB_NAME = stb_app
 
# vala compiler
VALAC = valac

# valadoc compiler
VALADOC = valadoc

# valadoc output directory
DIRECTORY = /home/STB_demo_documentation
 
# compiler options for a debug build
VALACOPTS = -g --thread
 
# set this as root makefile for Valencia
BUILD_ROOT = 1
 
# the 'all' target build a debug build
all:
	@$(VALAC) $(VALACOPTS) $(SRC) -o $(PROGRAM) $(PKGS) --disable-warnings
 
# the 'release' target builds a release build
# you might want to disabled asserts also
release: 
	@$(VALAC) -X -O2 $(SRC) -o main_release $(PKGS)
	
doc:
	@$(VALADOC) $(SRC) -o $(DIRECTORY) $(PKGS) --private --force

c-code:
	valac -C --vapidir=/opt/amino/freedom-sdk-0.1/freedom-cross-toolchain/sys-root/usr/share/vala-0.14/vapi/ $(SRC) $(PKGS) --thread

stb-app:
	i586-meego-linux-gnu-gcc -B /opt/amino/freedom-sdk-0.1/freedom-cross-toolchain/sys-root/usr/lib/ $(SRC_C) -o $(STB_NAME) $(STB_LIBS)

stb-app-vala:
	valac --cc i586-meego-linux-gnu-gcc $(SRC) $(PKGS)
 
# clean all built files
clean: 
	@rm -v -fr *~ *.c $(PROGRAM)

test:
	valac test.vala -o unit_test

