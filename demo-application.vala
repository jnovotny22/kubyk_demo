using GLib;
using Gee;
using Clutter;
using Mx;
using Anf.Stb.Library;
using Anf.Stb.Library.Common;
using Anf.Stb.Library.Common.Collections;
using Anf.Stb.Library.UI;
using Anf.Stb.Library.UI.Composite;
using Anf.Stb.Library.UI.Composite.Workspaces;

namespace Anf.Stb.Demo
{
	public class DemoApplication : Anf.Stb.Library.UI.Composite.Application
	{
		protected override void register_components(WorkItem workitem)
		{
			base.register_components(workitem);
			var stage = main_stage;
			main_stage.set_color(Color.from_string("black"));
			main_stage.set_width(1280);
			main_stage.set_height(720);
			var main_workspace = new DeckWorkspace();
			workitem.workspaces.set(Infrastructure.WorkspaceNames.MAIN_WORKSPACE, main_workspace);
			main_workspace.width = 1280;
			main_workspace.height = 720;
			main_workspace.set_position(0, 0);
			stage.add(main_workspace);
		}

		protected override Controller? create_main_controller(WorkItem workitem)
		{
			return new Controllers.MainController(workitem);
		}
	}
}

