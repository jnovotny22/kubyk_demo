using GLib;
using Gee;
using Clutter;
using Mx;
using Anf.Stb.Library.Common;
using Anf.Stb.Library.Common.Collections;
using Anf.Stb.Library.UI;
using Anf.Stb.Library.UI.Composite;
using Anf.Stb.Library.UI.Composite.Workspaces;

namespace Anf.Stb.Demo
{          
	public class Program
	{
		public static void main(string[] args)
		{
			var application = new DemoApplication();
			application.run(ref args);
		}
	}
}
