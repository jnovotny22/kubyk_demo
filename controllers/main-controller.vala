using Anf.Stb.Library;
using Anf.Stb.Library.Common;
using Anf.Stb.Library.Common.Collections;
using Anf.Stb.Library.UI;
using Anf.Stb.Library.UI.Composite;

namespace Anf.Stb.Demo.Controllers
{
	public class MainController : Controller
	{
		public MainController(WorkItem workitem)
		{
			base(workitem);

			var hello_world_command = new Anf.Stb.Library.UI.Composite.Commands.DelegatingCommand(
				() => { message("Hello world"); },
				() => { return true; });
			message("Command created");
			workitem.commands.set("HelloWorldCommand", hello_world_command);
			assert(workitem.commands.get("HelloWorldCommand") != null);
			message("Command registered");
		}

		protected override void on_activated()
		{
			message("Controller activated");
			var main_workspace = workitem.workspaces.get(Infrastructure.WorkspaceNames.MAIN_WORKSPACE);
			assert(main_workspace != null);
			var view = new Views.MainView();
			var presenter = new Views.MainViewPresenter(view, workitem);
			workitem.views.set(Infrastructure.WorkspaceNames.MAIN_WORKSPACE, view);
			main_workspace.show_view(view); 
		}
	}
}
