using Soup;
using GLib;

namespace Anf.Stb.Services
{
	//-----------------------------------------------------------------------------------
	// EROOR DOMAINS
	public errordomain HttpManagerError
	{
		NOT_FOUND,
		EMPTY_REPLY,
		EMPTY_URL
	}
	//-----------------------------------------------------------------------------------
	
	
	public class HttpManager:GLib.Object
	{		
		Soup.Session session;
		Soup.Message message;
		public uint http_session_timeout {get; set; default=5;}
		public uint8[] reply{get; private set;}
		
		
		//---------------------------------------------------------------------------
		public HttpManager()
		{
			session = new SessionSync();
		}
		
		//---------------------------------------------------------------------------
		public void send_request(string url) throws HttpManagerError
		{			
			
			if(url == "")
			{
				throw new HttpManagerError.EMPTY_URL("Address not valid!");
			}
			GLib.message("Sending request to URL: " + url);
			message = new Message("GET", url);
			session.timeout = http_session_timeout;
			session.send_message(message);
    			reply = message.response_body.data;
    			  			
    			string text_reply = (string)  message.response_body.data;
    			
    			if(reply.length == 0)
    			{
    				reply = null;
    				throw new HttpManagerError.EMPTY_REPLY("The reply for " + url + " is epmty");
    			}
    			
    			if(text_reply.to_string().contains("<title>404 Not Found</title>"))
    			{
    				reply = null;
    				throw new HttpManagerError.NOT_FOUND(url + "address does not exist!");
    			}
    			
    			GLib.message("Download complete: " + url);
		}
		
		//---------------------------------------------------------------------------
	}
	
	
	
	public class HttpManagerAsync:GLib.Object
	{		
		Soup.Session session;
		Soup.Message message;
		public uint8[] reply{get; private set;}
		public uint http_session_timeout {get; set; default=5;}
		public signal void download_completed(int id);
		public string request_url {get; private set; default="";}
		
		//---------------------------------------------------------------------------
		public HttpManagerAsync()
		{
			session = new SessionAsync();
		}
		
		//---------------------------------------------------------------------------
		public async void send_request(string url, int id=0) throws HttpManagerError, ThreadError
		{
			if(url == "")
			{
				throw new HttpManagerError.EMPTY_URL("Address not valid!");
			}
			this.request_url = url;
			debug("Sending request to URL: " + url);
			SourceFunc callback = send_request.callback;
			message = new Message("GET", url);
			session.timeout = http_session_timeout;
			
			Idle.add(send_request.callback);
			yield;
			session.send_message(message);
    			reply = message.response_body.data;
    			
    			if(reply.length == 0)
    			{
    				reply = null;
    				throw new HttpManagerError.EMPTY_REPLY("The reply for " + url + " is epmty");
    			}
    			  			
    			string text_reply = (string)  message.response_body.data;
    			
    			if(text_reply.to_string().contains("<title>404 Not Found</title>"))
    			{
    				reply = null;
    				throw new HttpManagerError.NOT_FOUND(url + "address does not exist!");
    			}
    			
    			GLib.message("Download complete: " + url);
    			download_completed(id);
		}
		
		//---------------------------------------------------------------------------
	}
	
	
	
}
