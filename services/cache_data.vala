using GLib;
 
namespace Anf.Stb.Services
{
	class CacheData
	{
		public static const string CACHE_PATH = "./cache/";
		
		public static void create_directory_if_not_exists(string directory_name)
		{
			try
			{
				File file = File.new_for_path(CACHE_PATH + directory_name);
				if(!file.query_exists())
				{
					file.make_directory_with_parents();
					message("Directory " + directory_name + "created!");
				}
				
			}
			catch(Error e)
			{
				critical(e.message);
			}
		}
		
		public static void cache_data(string file_name, uint8[] data, bool overwrite_if_exists=true)
		{
			try
			{
				if(file_name == "")
					return;
					
				var file = File.new_for_path(CACHE_PATH + file_name);
				
				if(file.query_exists())
				{
					if(overwrite_if_exists)
						file.delete();
					else
						return;
				}
				
				var file_stream = file.create(FileCreateFlags.NONE);
				var data_stream = new DataOutputStream(file_stream);
				
				long written = 0;
				while (written < data.length)
				{
					written += data_stream.write(data[written:data.length]);	
				}
				message("Data cached into: " + file_name);
			}
			catch (Error e) 
			{
			        critical(e.message);
    			}
		}


		public static bool is_cached(string directory, string filename, out File? cached_file)
		{
			cached_file = File.new_for_path(CacheData.CACHE_PATH +  directory + filename);
			if(cached_file.query_exists())
			{
				return true;
			}
			else
			{
				cached_file = null;
				return false;
			}
		}
	}
}
