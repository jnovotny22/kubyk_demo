using GLib;
using Gee;
using Xml;

namespace Anf.Stb.Services
{
	public class XmlParser
	{
		
		bool debugMode;
		private int indent = 0;
		private int listIter = 0;
		public ArrayList<HashMap<string, string>> parsedList;
		HashMap<string, string> map; 
		
		
		public XmlParser()
		{
			parsedList = new ArrayList<HashMap<string, string>> ();
			debugMode = true;
		} 
		
		//---------------------------------------------------------------------------
		// Method for parsing XML files
		public void parse_file (string path) 
		{
        		// Parse the document from path
		        Xml.Doc* doc = Parser.parse_file (path);
		        if (doc == null) {
		                critical("File %s not found or permissions missing", path);
        	    		return;
        		}
	        	// Get the root node. notice the dereferencing operator -> instead of .
        		Xml.Node* root = doc->get_root_element ();
        		if (root == null) {
        		    // Free the document manually before returning
        		    delete doc;
        		    critical("The xml file '%s' is empty", path);
        		    return;
        		}
			
			parsedList.add(new HashMap<string, string> ());

        		// Let's parse those nodes
        		parse_node (root);
			
			parsedList.remove_at(parsedList.size - 1);
        		// Free the document
        		delete doc;
    		}
    		
    		//---------------------------------------------------------------------------
    		// method for parsing modes
    		private void parse_node (Xml.Node* node) {
        		this.indent++;
        		// Loop over the passed node's children
        		for (Xml.Node* iter = node->children; iter != null; iter = iter->next) {
        		    	   		    	
        		    	// Spaces between tags are also nodes, discard them
        		    	if (iter->type != ElementType.ELEMENT_NODE) {
        		        	continue;
        		    	}
				
        			// Get the node's name-
        			string node_name = iter->name;
            			// Get the node's content with <tags> stripped
			        string node_content = iter->get_content ();
            			parsedList[listIter].set(node_name, node_content);
            			
			        // Now parse the node's properties (attributes) ...
				//parse_properties (iter);
				// Followed by its children nodes
				parse_node (iter);
        		}
        		
        		this.indent--;
        		if(this.indent == 1)
        		{
        			parsedList.add(new HashMap<string, string> ());
        			listIter ++;
        		}
    		}
    		
    		//---------------------------------------------------------------------------
    		// method for parsing attributes
		//private void parse_properties (Xml.Node* node) {
        	//	// Loop over the passed node's properties (attributes)
		//        for (Xml.Attr* prop = node->properties; prop != null; prop = prop->next) {
		//            string attr_name = prop->name;

		            // Notice the ->children which points to a Node*
			    // (Attr doesn't feature content)
		//            string attr_content = prop->children->content;
        	//	}
	    	//}	
	}
}

