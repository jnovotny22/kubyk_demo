using GLib;
using Gee;

namespace Anf.Stb.Services
{
	public class MainMenuDownloader : IMenuDownloader
	{		
		public XmlParser xml_parser;

		public MainMenuDownloader()
		{

		}
		public int download_data(out ArrayList<string> icon_paths, out ArrayList<string> selected_icon_paths)
		{  
			HttpManager http_manager = new HttpManager();
            try {
                message("Downloading main menu items");
                
                string configuration_file_path = "http://78.46.46.14/EPG/menu/main_menu_heimat.xml";
                string[] configuration_file_name_array = configuration_file_path.split("/");
                string configuration_file_name = configuration_file_name_array[configuration_file_name_array.length - 1];
                http_manager.send_request(configuration_file_path);
                CacheData.cache_data(configuration_file_name, http_manager.reply);
                message("Main menu XML configuration downloaded and cached!");                
                message("Parsing main menu configuration XML...");
                xml_parser = new XmlParser();
                xml_parser.parse_file(CacheData.CACHE_PATH + configuration_file_name);
                message("Main menu configurations parsed!");
            }
            catch (Error e) {
                message("Main menu singleton error: " + e.message);
                xml_parser = new XmlParser();
                xml_parser.parsedList = new ArrayList<HashMap<string, string>>();
            }	
			string[] _icon;	
			string image_name;	
			int icon_count=0;	
			foreach (HashMap<string, string> map in xml_parser.parsedList)
			{ 
				_icon = map["icon"].split("/");
				image_name = _icon[_icon.length-1];
				http_manager.send_request(map["icon"]);				
				CacheData.cache_data(image_name, http_manager.reply, false);
				icon_paths.add(image_name);
				
				_icon = map["icon_selected"].split("/");
				image_name = _icon[_icon.length-1];
				http_manager.send_request(map["icon_selected"]);	
				CacheData.cache_data(image_name, http_manager.reply, false);
				selected_icon_paths.add(image_name);
				icon_count++;
			}
			return icon_count;
		}
	}
}
